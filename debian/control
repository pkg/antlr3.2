Source: antlr3.2
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Ludovic Claude <ludovic.claude@laposte.net>,
           Emmanuel Bourg <ebourg@apache.org>,
           Jakub Adam <jakub.adam@ktknet.cz>
Build-Depends: bnd (>= 2.1.0),
               cdbs,
               debhelper (>= 10),
               default-jdk,
               default-jdk-doc,
               junit4,
               libantlr-maven-plugin-java (>= 2.1),
               libmaven-install-plugin-java,
               libmaven-javadoc-plugin-java,
               libmaven-plugin-testing-java,
               libmaven-plugin-tools-java,
               libstringtemplate-java (>= 3.2.1),
               maven-debian-helper (>= 1.1)
Standards-Version: 4.0.0
Vcs-Git: https://anonscm.debian.org/git/pkg-java/antlr3.2.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/antlr3.2.git
Homepage: http://www.antlr3.org

Package: antlr3.2
Architecture: all
Depends: default-jre-headless | java2-runtime-headless |
 java5-runtime-headless | java6-runtime-headless,
         libstringtemplate-java,
         ${misc:Depends}
Recommends: ${maven:OptionalDepends}
Breaks: antlr3 (<< 3.3~)
Replaces: antlr3 (<< 3.3~)
Description: language tool for constructing recognizers, compilers etc
 ANTLR, ANother Tool for Language Recognition, (formerly PCCTS) is
 a language tool that provides a framework for constructing recognizers,
 compilers, and translators from grammatical descriptions containing C++
 or Java actions [You can use PCCTS 1.xx to generate C-based parsers].
 .
 Computer language translation has become a common task. While
 compilers and tools for traditional computer languages (such as C
 or Java) are still being built, their number is dwarfed by the thousands
 of mini-languages for which recognizers and translators are being
 developed. Programmers construct translators for database formats,
 graphical data files (e.g., PostScript, AutoCAD), text processing
 files (e.g., HTML, SGML).  ANTLR is designed to handle all of your
 translation tasks.

#Package: antlr3.2-doc
#Architecture: all
#Section: doc
#Depends: default-jdk-doc, ${misc:Depends}
#Suggests: libantlr3.2-java
#Breaks: antlr3-doc (<< 3.3~)
#Replaces: antlr3-doc (<< 3.3~)
#Description: language tool for constructing compilers etc - documentation
# ANTLR, ANother Tool for Language Recognition, (formerly PCCTS) is
# a language tool that provides a framework for constructing recognizers,
# compilers, and translators from grammatical descriptions containing C++
# or Java actions [You can use PCCTS 1.xx to generate C-based parsers].
# .
# This package provides the API documentation for ANTLR 3

Package: antlr3.2-maven-plugin
Architecture: all
Depends: antlr3.2 (>= ${source:Version}),
         libmaven3-core-java,
         libplexus-compiler-java,
         ${misc:Depends}
Breaks: antlr3-maven-plugin (<< 3.3~)
Replaces: antlr3-maven-plugin (<< 3.3~)
Description: Maven plugin for ANTLR 3.2
 ANTLR, ANother Tool for Language Recognition, (formerly PCCTS) is
 a language tool that provides a framework for constructing recognizers,
 compilers, and translators from grammatical descriptions containing C++
 or Java actions [You can use PCCTS 1.xx to generate C-based parsers].
 .
 This package provides the Maven plugin that supports compiling ANTLR 3
 grammars during a Maven build

Package: libantlr3.2-gunit-java
Architecture: all
Depends: antlr3.2 (>= ${source:Version}), junit4, ${misc:Depends}
Suggests: antlr3.2-gunit-maven-plugin
Breaks: libantlr3-gunit-java (<< 3.3~)
Replaces: libantlr3-gunit-java (<< 3.3~)
Description: Unit Test framework for ANTLR grammars
 gUnit is a "Unit Test" framework for ANTLR grammars. It provides a simple
 way to write and run automated tests for ANTLR grammars in a manner similar
 to Java unit testing framework jUnit. The basic idea is to create a bunch of
 input/output pairs for rules in a grammar and gUnit will verify the expected
 output/result. The input can be a single line or multiple lines of strings or
 even an external file. The output can be simply success or failure, an
 abstract syntax tree (AST), a rule return value, or some text output which
 could be a rule's template return value. The current version of gUnit has 2
 main functions, interpreter and jUnit generator. The interpreter interprets
 your gUnit script and runs unit tests using Java reflection to invoke methods
 in your parser objects. The generator, on the other hand, translates your
 gUnit script to jUnit Java code that you can compile and execute by hand.

#Package: libantlr3.2-gunit-java-doc
#Architecture: all
#Section: doc
#Depends: antlr3.2-doc, junit4-doc, ${misc:Depends}
#Suggests: libantlr3.2-gunit-java
#Description: API documentation for gUnit
# gUnit is a "Unit Test" framework for ANTLR grammars. It provides a simple
# way to write and run automated tests for ANTLR grammars in a manner similar
# to Java unit testing framework jUnit. The basic idea is to create a bunch of
# input/output pairs for rules in a grammar and gUnit will verify the expected
# output/result. The input can be a single line or multiple lines of strings or
# even an external file. The output can be simply success or failure, an
# abstract syntax tree (AST), a rule return value, or some text output which
# could be a rule's template return value. The current version of gUnit has 2
# main functions, interpreter and jUnit generator. The interpreter interprets
# your gUnit script and runs unit tests using Java reflection to invoke methods
# in your parser objects. The generator, on the other hand, translates your
# gUnit script to jUnit Java code that you can compile and execute by hand.
# .
# This package provides the API documentation for gUnit for ANTLR 3

Package: antlr3.2-gunit-maven-plugin
Architecture: all
Depends: libantlr3.2-gunit-java (>= ${source:Version}),
         libmaven3-core-java,
         libplexus-compiler-java,
         ${misc:Depends}
Breaks: antlr3-gunit-maven-plugin (<< 3.3~)
Replaces: antlr3-gunit-maven-plugin (<< 3.3~)
Description: Maven plugin for gUnit, a unit test framework for ANTLR grammars
 gUnit is a "Unit Test" framework for ANTLR grammars. It provides a simple
 way to write and run automated tests for ANTLR grammars in a manner similar
 to Java unit testing framework jUnit. The basic idea is to create a bunch of
 input/output pairs for rules in a grammar and gUnit will verify the expected
 output/result. The input can be a single line or multiple lines of strings or
 even an external file. The output can be simply success or failure, an
 abstract syntax tree (AST), a rule return value, or some text output which
 could be a rule's template return value. The current version of gUnit has 2
 main functions, interpreter and jUnit generator. The interpreter interprets
 your gUnit script and runs unit tests using Java reflection to invoke methods
 in your parser objects. The generator, on the other hand, translates your
 gUnit script to jUnit Java code that you can compile and execute by hand.
 .
 This package provides the Maven plugin that allows one to run gUnit tests
 during a Maven build.
