antlr3.2 (3.2-16+apertis1) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 22 Feb 2022 17:22:08 +0530

antlr3.2 (3.2-16co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Thu, 11 Feb 2021 00:42:01 +0000

antlr3.2 (3.2-16) unstable; urgency=medium

  * Team upload.
  * Depend on libmaven3-core-java instead of libmaven2-core-java

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 02 Aug 2017 11:55:48 +0200

antlr3.2 (3.2-15) unstable; urgency=medium

  * Team upload.
  * Added the missing build dependency on junit4 (Closes: #866550)
  * Standards-Version updated to 4.0.0
  * Switch to debhelper level 10
  * Use a secure Vcs-Git URL

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 30 Jun 2017 09:21:54 +0200

antlr3.2 (3.2-14) unstable; urgency=medium

  * Use the plexus-compiler-api 2.x to fix a build failure with Maven 3
  * Fixed the dependency cycle between antlr and antlr-maven-plugin

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 14 Dec 2015 16:17:58 +0100

antlr3.2 (3.2-13) unstable; urgency=medium

  * Team upload.
  * Transition to bnd 2.1.0.
  * Vcs-Browser: Use https.
  * Fix spelling error in package description.

 -- Markus Koschany <apo@debian.org>  Thu, 19 Nov 2015 19:44:52 +0100

antlr3.2 (3.2-12) unstable; urgency=medium

  * Cloned the package as antlr3.2 since some packages are incompatible
    with ANTLR 3.5
  * debian/watch: Watch the release tags on Github

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 03 Sep 2015 12:06:01 +0200

antlr3 (3.2-11) unstable; urgency=medium

  * Team upload.
  * Replace dependency on dummy package libplexus-compiler-api-java
    with libplexus-compiler-java (Closes: #791487)
  * Moved the package to Git
  * Bump Standards-Version to 3.9.6 (no changes).

 -- tony mancill <tmancill@debian.org>  Sat, 11 Jul 2015 10:58:15 -0700

antlr3 (3.2-10) unstable; urgency=medium

  * Fixed a NullPointerException in CompositeGrammar.getIndirectDelegates()
    with Java 8
  * Standards-Version updated to 3.9.5 (no changes)
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 07 Apr 2014 14:22:17 +0200

antlr3 (3.2-9) unstable; urgency=low

  * Team upload.

  [ Jakub Adam ]
  * Add OSGi metadata to antlr3.jar and antlr3-runtime.jar.
  * Add Jakub Adam to Uploaders.

  [ tony mancill ]
  * Clean up lintian warning about BSD license in debian/copyright.

 -- tony mancill <tmancill@debian.org>  Sun, 29 Sep 2013 09:38:53 -0700

antlr3 (3.2-8) unstable; urgency=low

  * Updated Standards-Version to 3.9.4 (no changes)
  * Updated the watch file
  * Converted debian/copyright to the DEP5 format
  * Added a clean target to debian/rules to allow rebuilds
  * Use canonical URLs for the Vcs-* fields
  * Fixed syntax issues in the manpages

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 03 Jul 2013 13:27:44 +0200

antlr3 (3.2-7) unstable; urgency=low

  * Team upload.

  [ tony mancill ]
  * Remove Michal Koch from Uploaders. (Closes: #653985)

  [ James Page ]
  * Fix FTBFS with openjdk-7:
    - d/patches/java7-compat.patch: Target 1.5 instead of jsr14 which is
      not supported by Java 7.

 -- James Page <james.page@ubuntu.com>  Sat, 23 Jun 2012 13:14:52 +0200

antlr3 (3.2-6) unstable; urgency=low

  * Team upload.
  * Add missing Build-Depends: libmaven-plugin-tools-java. (Closes: #643493)
  * Do no longer build antlr3-gcj.

 -- Torsten Werner <twerner@debian.org>  Tue, 27 Sep 2011 22:49:38 +0200

antlr3 (3.2-5) unstable; urgency=low

  * Team upload.
  * d/rules: Set DEB_MAVEN_BUILD_TARGET := package install
    so that JAR will be installed in temporary maven repository.
    This should please javadoc:jar plugin. (Closes: #606294)
    (ie. it won't try to download antlr-gunit module from network).
  * d/control: Build-Depends on libmaven-install-plugin-java to get
    "install" lifecycle working.

 -- Damien Raude-Morvan <drazzib@debian.org>  Sat, 08 Jan 2011 01:55:34 +0100

antlr3 (3.2-4) unstable; urgency=low

  [ Ludovic Claude ]
  * Rebuild with a newer version of maven-debian-helper (Closes: #587499)
  * maven.rules: transform antlr 3 versions to '3.x' instead of 'debian'
  * Split packaging of gUnit jar into new libantlr3-gunit-java package
    and gUnit Maven plugin into new antlr3-gunit-maven-plugin package,
    split Maven plugin for Antlr into new antlr3-maven-plugin package
    (Closes: #588005) 
  * Update dependencies of antlr3, remove the dependencies coming from
    those 3 new packages.

  [ Niels Thykier ]
  * Fixed a typo in the control so antlr3 now properly suggests
    antlr3-gcj.
  * Reduced the alternative JREs to headless versions.
  * Bumped Standards-Versions to 3.9.0 - no changes required.
  * Replaced B-D on default-jdk-builddep with gcj-native-helper
    and default-jdk.

 -- Ludovic Claude <ludovic.claude@laposte.net>  Wed, 21 Jul 2010 23:22:09 +0200

antlr3 (3.2-3) unstable; urgency=low

  * Clean up build dependencies. (Closes: #587401)

 -- Torsten Werner <twerner@debian.org>  Mon, 28 Jun 2010 20:49:51 +0200

antlr3 (3.2-2) unstable; urgency=low

  [ Ludovic Claude ]
  * New upstream (Closes: #579504)
  * debian/watch fixes in previous version (Closes: #570684)
  * Add missing antlr3-runtime.jar library to antlr3 wrapper script
    (Closes #587212)
  * Also don't export the CLASSPATH variable, there's no need to affect
    the whole environement when launching this program
  * Add man page for antlr3 script

  [ Torsten Werner ]
  * Call dh_nativejava to fill the antlr3-gcj package again. (Closes: #587306)
  * Reformat a changelog entry for the last version.

 -- Torsten Werner <twerner@debian.org>  Sun, 27 Jun 2010 21:01:36 +0200

antlr3 (3.2-1) unstable; urgency=low

  [ Ludovic Claude ]
  * Upstream build now uses Maven
  * debian/control: 
      - Updated Standards-Version to 3.8.4.
      - Build-Depends-Indep: add dependencies for Maven build: 
        libantlr-maven-plugin-java, maven-debian-helper,
        libmaven-site-plugin-java, libstringtemplate-java,
        libmaven-javadoc-plugin-java
      - Build-Depends-Indep: add dependencies to build the Javadoc:
        default-jdk-doc, libmaven-javadoc-plugin-java
      - antlr3 Depends: remove java1-runtime, add java5-runtime
      - downgrade antlr3-gcj from Recommands: to Suggests:
      - create an antlr3-doc package that contains the documentation
      [TODO] create an antlr3-gunit package and modify maven-debian-helper to
      deploy the gunit jars to this package
  * debian/rules: remove include simple-patchsys.mk, replaced by
    source format 3.0 (quilt)
  * Add orig-tar to clean up upstream sources
  * Switch to source format 3.0 (quilt)
  
  [ Michael Koch ]
  * New upstream version (Closes: #511470).
  * debian/watch: Fixed by using site-local absolution download link.
  * debian/control: Updated Standards-Version to 3.8.3.
  * debian/control: Added Homepage field.

  [ Torsten Werner ]
  * Remove Build-Depends: quilt.

 -- Torsten Werner <twerner@debian.org>  Tue, 22 Jun 2010 14:53:06 +0200

antlr3 (3.0.1+dfsg-4) unstable; urgency=low

  * Fix build failure for binary-arch only builds. Closes: #502660.
  * Compile java files with -source 1.4.

 -- Matthias Klose <doko@debian.org>  Thu, 23 Oct 2008 01:32:35 +0200

antlr3 (3.0.1+dfsg-3) unstable; urgency=low

  * antlr3: Depend on default-jre-headless.
  * Build an antlr3-gcj package.

 -- Matthias Klose <doko@debian.org>  Mon, 29 Sep 2008 23:18:19 +0200

antlr3 (3.0.1+dfsg-2) unstable; urgency=low

  * Add depends on libstringtemplate-java. Closes: #474719
  * Add wrapper script for antlr. Closes: #474721

 -- Michael Koch <konqueror@gmx.de>  Tue, 08 Apr 2008 20:35:27 +0200

antlr3 (3.0.1+dfsg-1) unstable; urgency=low

  * Repackaged orig tarball.

 -- Michael Koch <konqueror@gmx.de>  Mon, 31 Mar 2008 23:50:00 +0200

antlr3 (3.0.1-1) unstable; urgency=low

  * Initial version. Closes: #439725

 -- Michael Koch <konqueror@gmx.de>  Sun, 02 Mar 2008 01:40:30 +0100
